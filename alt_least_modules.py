'''
Este programa fatora uma matriz A utilizando 
o método dos mínimos quadrados alternados.
Dependências: 
    numpy
    sys
    math
Execução:
    python alt_least_squares.py a_matrix.txt p
Entrega (retorno):
    A matriz W, depois de feita a solução do sistema WH = A
'''

import numpy as np
import sys
import math


#####################################################################
# Calcula o erro quadrático entre A e uma fatoração WH
def find_error(mx0, mx1, mx2): # find_error(A, W, H)
    mx = np.subtract(mx0, np.matmul(mx1, mx2))   
    e = np.sum(mx ** 2, axis=None)
    return e

# Triangulariza o sistema mx*V = b_mx, em que v será a matriz das incógnitas.
def triangulate(mx, b_mx):
    n = mx.shape[0]
    m = mx.shape[1]
    
    if len(b_mx.shape) > 1:
        m_b = b_mx.shape[1]
    else:
        m_b = 1    

    for k in range(m):
        for j in range(n-1, k, -1):
            i = j - 1
            wi = mx[i][k]
            wj = mx[j][k]
            if wj != 0:
                if np.abs(wi) > np.abs(wj):
                    t = np.true_divide(-wj, wi)
                    c = 1/np.sqrt(1 + t**2)
                    s = np.multiply(c,t)
                else:
                    t = np.true_divide(-wi, wj)
                    s = 1/np.sqrt(1 + t**2)
                    c = np.multiply(s,t)
                # aplicando as transformações por rotações de Givens
                mx[i, 0:m], mx[j, 0:m] = (np.subtract(c*mx[i, 0:m], s*mx[j, 0:m]), np.add(s*mx[i, 0:m], c*mx[j, 0:m]))
                
                b_mx[i, 0:m_b], b_mx[j, 0:m_b] = (np.subtract(c*b_mx[i, 0:m_b], s*b_mx[j, 0:m_b]), np.add(s*b_mx[i, 0:m_b], c*b_mx[j, 0:m_b]))
                
    return mx, b_mx

def solve(mx, b_mx):
    mx , b_mx = triangulate(mx, b_mx)
    n = mx.shape[0] # número de linhas de mx = número de linhas de b_mx
    p = mx.shape[1] # número de colunas de mx = número de linhas de H
    if len(b_mx.shape) != 1:
        m = b_mx.shape[1]
    else:
        m = 1 # número de colunas de b_mx = número de colunas de H
    
    # Resolve o sistema que já está triangularizado
    # determina os primeiros valores:
    H = np.array([[0.0]*m]*p)
    H[p-1] = np.true_divide(b_mx[p-1], mx[p-1][p-1])
    
    for k in range(p-2, -1, -1): # percorre as linhas, subindo
        for t in range(k+1, p):
            b_mx[k] = b_mx[k] - np.multiply(mx[k][t], H[t])
        H[k] = np.true_divide(b_mx[k], mx[k][k])
    return H

# Decompõe uma matriz A(n,m) em uma W(n,p) e outra H(p,m)
def decompose(A, p): 
    n = A.shape[0]
    W = np.random.rand(n,p)
    H = np.random.rand(p,A.shape[1])
    epsilon = 10**-5
    error = 1.
    delta = 0.5 # um valor qualquer para entrar no loop

    itmax = 100
    it = 0
    
    while delta > epsilon and it < itmax:
        #passo 1: normaliza W
        for k in range(p):
            aux = np.sqrt(np.sum(W[0:n, k] ** 2, axis=None))
            W[0:n, k] = np.true_divide(W[0:n, k], aux)
        
        #passo 2
        Ac = np.copy(A)     
        H = solve(W, Ac)
        
        #passo 3
        H = np.where(H > 0, H, 10**-6)
        
        #passo 4
        At = np.transpose(A).copy()
        Ht = np.transpose(H).copy()
        
        #passo 4
        Wt = solve(Ht, At)
        
        #passo 5
        W = np.transpose(Wt).copy()
        W = np.where(W > 0, W, 10**-6)
        
        it = it + 1
        delta = find_error(A.copy(), W.copy(), H) - error
        error = delta + error
        delta = abs(delta)

    return W

#####################################################################
