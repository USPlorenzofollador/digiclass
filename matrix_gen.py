'''
Este programa imprime as matrizes requeridas para testar o 
programa givens_Rot.py e alt_least_squares.py, conforme
especificadas na seção Primeira Tarefa.
Dependências:
    sys
    random
Execução:
    python matrix_gen.py letraX
Sendo X = a,b,c,d
Entrega (retorno):
    Cria os arquivos w_matrix.txt e b_matriz.txt
'''

import sys
import random

###########################################################
def build_a():
    for i in range(64):
        for j in range(64):
                mdl = abs(i - j)
                if mdl == 0:
                    file.write("%d" % 2)
                elif mdl == 1:
                    file.write("%d" % 1)
                else:
                    file.write("%d" % 0)
                if j < 63:
                    file.write(" ")
        file.write("\n")
        
def build_b():
    for i in range(20):
        for j in range(17):
            mdl = abs(i - j)
            if mdl < 5:
                file.write("%s" % str(1/(i + j + 1)))
            else:
                file .write("%d" % 0)
            if j < 16:
                file.write(" ")
        file.write("\n")
#########################################################

if sys.argv[1] == "letraa":
    file = open("w_matrix.txt", "w+")
    b_mx = open("b_matrix.txt", "w+")
    build_a()
    for i in range(64):
        b_mx.write("%d\n" % 1)
    file.close()
    b_mx.close()


if sys.argv[1] == "letrab":
    file = open("w_matrix.txt", "w+")
    b_mx = open("b_matrix.txt", "w+")
    build_b()
    for i in range(20):
        b_mx.write("%d\n" % (i+1))
    file.close()
    b_mx.close()

if sys.argv[1] == "letrac":
    file = open("w_matrix.txt", "w+")
    b_mx = open("b_matrix.txt", "w+")
    build_a()
    for i in range(64):
        b_mx.write("%d " % 1)
        b_mx.write("%d " % (i + 1))
        b_mx.write("%d\n" % (2*i + 1))
    file.close()
    b_mx.close()
        
if sys.argv[1] == "letrad":
    file = open("w_matrix.txt", "w+")
    b_mx = open("b_matrix.txt", "w+")
    build_b()
    for i in range(20):
        b_mx.write("%d " % 1)
        b_mx.write("%d " % (i + 1))
        b_mx.write("%d\n" % (2*i + 1))  
    file.close()
    b_mx.close()
    
if len(sys.argv) == 3:
    file = open("w_matrix.txt", "w+")
    n = int(sys.argv[1])
    m = int(sys.argv[2])
    print("Created matrix, size", n, "x" , m)
    for i in range(n):
        for j in range(m-1):
            file.write("%f " % random.random())
        file.write("%f" % random.random())
        file.write("\n")
    file.close()         

