'''
Este programa resolve sistemas simultaneos com matrizes dadas
utilizando o método de rotação de Givens.
Dependências:
    numpy
    math 
    sys
Execução:
    python givens_rot.py
Entrega (retorno):
    matriz H, solução do sistema
'''
import numpy as np
import math
import sys

################################################################3
def Rot_givens(mx, i, j, c, s):
    mxc = np.copy(mx)
    
    mxc[i] = np.multiply(mx[i], c) - np.multiply(mx[j], s)
    mxc[j] = np.multiply(mx[i], s) + np.multiply(mx[j], c)
    
    return mxc
    
def triangulate(mx, b_mx):
    n = mx.shape[0]
    m = mx.shape[1]
    
    mxc = np.copy(mx)
    b_mxc = np.copy(b_mx)
    
    for k in range(m):
        for j in range(n-1, k, -1):
            i = j - 1
            wi = mxc[i][k]
            wj = mxc[j][k]
            if wj != 0:
                if np.abs(wi) > np.abs(wj):
                    t = np.true_divide(-wj, wi)
                    c = 1/np.sqrt(1 + t**2)
                    s = np.multiply(c,t)
                else:
                    t = np.true_divide(-wi, wj)
                    s = 1/np.sqrt(1 + t**2)
                    c = np.multiply(s,t)
                # aplicando transformação
                mxc   = Rot_givens(mxc, i, j, c, s)
                b_mxc = Rot_givens(b_mxc, i, j, c, s)
    return mxc, b_mxc

def solve(mx, b_mx):
    mx , b_mx = triangulate(mx, b_mx)
    n = mx.shape[0] # número de linhas de mx = número de linhas de b_mx
    p = mx.shape[1] # número de colunas de mx = número de linhas de H
    if len(b_mx.shape) != 1:
        m = b_mx.shape[1]
    else:
        m = 1 # número de colunas de b_mx = número de colunas de H
    
    # determina os primeiros valores:
    H = np.array([[0.0]*m]*p)
    H[p-1] = np.true_divide(b_mx[p-1], mx[p-1][p-1])
    
    for k in range(p-2, -1, -1): # percorre as linhas, subindo
        for t in range(k+1, p):
            b_mx[k] = b_mx[k] - np.multiply(mx[k][t], H[t])
        H[k] = np.true_divide(b_mx[k], mx[k][k])
    return H

###################################################################

w_path = input('Forneça arquivo com matriz W: ')
b_path = input('Forneça arquivo com matriz b: ')
w_mx   = np.loadtxt(w_path)
b_mx   = np.loadtxt(b_path)
h = solve(w_mx, b_mx)
print(h)

