# DigiClass
**Classificador de dígitos por machine learning, em python.** 

![Imagem](cover.jpg)

### main_ex.py
Esse programa pretende resolver a tarefa principal deste projeto: classificar dígitos manuscritos.
Ele utiliza a fatoração por mínimos quadrados alternados.

O programa tem três funções: `train`, que "treina" a máquina, `class`, que classifica um conjunto de dados e  `compare`, que calcula a porcentagem de acerto da classificação com base em um gabarito fornecido.
Exemplos de execução:
#### Treinamento:
```
➜ python main_ex.py train
escolha p entre 5, 10 e 15: 15
escolha ndig_treino entre 100, 1000 e 4000: 4000
Training...
Finished training digit 0.   Elapsed time: 284.1 sec
Finished training digit 1.   Elapsed time: 547.1 sec
etc....
```
#### Classificação:
```
➜ python main_ex.py class
Escolha n_test (entre 1 e 10mil): 10000
Classifying...
Finished classification. 
To see results, run 'python3 main_ex.py compare' 
Elapsed time:  15.2 sec

```

#### Averiguação
```
➜ python main_ex.py compare
Comparing...
Acertos: 9370 de 10000
Porcentagem de acertos: 93.70 %
Acertos por dígito: 
Dígito 0: acertos: 968 |  98.78 %
Dígito 1: acertos: 1127 |  99.30 %
Dígito 2: acertos: 938 |  90.89 %
Dígito 3: acertos: 940 |  93.07 %
Dígito 4: acertos: 920 |  93.69 %
Dígito 5: acertos: 803 |  90.02 %
Dígito 6: acertos: 928 |  96.87 %
Dígito 7: acertos: 958 |  93.19 %
Dígito 8: acertos: 863 |  88.60 %
Dígito 9: acertos: 925 |  91.67 %

```

### alt_least_squares.py
Esse programa usa o método de mínimos quadrados alternados para fatorar a matriz de entrada A em um produto
WA, e devolve a matriz W (uma matriz nXp).
Suas funções `decompose` e `solve` são usadas no `main_ex.py`
Execução:

```python alt_least_squares.py a_matrix.txt p```


### givens_rot.py
Esse programa tem funções `Rot_givens(), tringulate(), solve()` que rotaciona, triangulariza e resolve os sistemas simultâneos.  
Para criar um arquivo matrix.txt de entrada, pode-se usar `matrixGen.py`
Exemplo de execução:
```
➜ python givens_rot.py 
Forneça arquivo com matriz W: matrix.txt
Forneça arquivo com matriz b: b_matrix.txt
```


### matrix_gen.py
Esse programa tem dois tipos de função:
##### **Modo n X m**
``` python matrixGen.py n m```

Produz um arquivo matrix.txt com uma matriz n por m, criada a partir de números aleatórios entre 0 e 10.
Pode-se usar o **modo n X m** duas vezes para criar as matrizes de coeficientes e de termos independentes, renomeando uma delas.

##### **Modo letrax**
``` python matrixGen.py letraa```

produz matrizes do tipo Wa. Se o argumento ```letrab``` for utilizado, o programa cria matrizes do tipo Wb. Idem para as
letras c e d do enunciado.




