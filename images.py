''' 
Mostrar ibagens
'''
import sys
import matplotlib.pyplot as plt
import numpy as np

print("Lendo arquivos")
for k in range(10):
    Wd_path = "Trained/trained_" + str(k) + ".txt" 
    Wd = np.loadtxt(Wd_path) # recebe a matriz treinada n por p. Cada coluna é uma imagem.
    B = Wd[:,0] # pega só a primeira coluna de Wd
    W = []
    for i in range(28):
        w = []
        for j in range(28):
            w.append(B[28*i + j])
        W.append(w)
    plt.gray()
    plt.imshow(W)
    print("Criando imagem do dígito", k)
    plt.savefig('images/IMG_trained_' + str(k) + '.png')

