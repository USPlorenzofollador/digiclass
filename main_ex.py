'''
Programa que treina e identifica padrões de escrita em imagens 784x784.
Execução:
    1. Treinamento:
        python main_ex.py train
    2. Classificação (n_test é o número de imagens que queremos classificar):
        python main_ex.py class n_test
    3. Averiguação de resultados
        python main_ex.py compare
'''
import numpy as np
import matplotlib.pyplot as plt
import math
import sys
import time
from alt_least_modules import decompose, solve

l = 28*28

'''
Parte 1 - Treinamento
Execução: python main_ex.py train
'''
if sys.argv[1] == "train":
    p = int(input("escolha p entre 5, 10 e 15: ")) # valores: 5, 10, 15
    ndig_treino = int(input("escolha ndig_treino entre 100, 1000 e 4000: ")) # valores: 100, 1000, 4000

    print("Training...")
    tic = time.time()
    for i in range(10):
        a_path = "dados_mnist/train_dig" + str(i) + ".txt"
        A = np.loadtxt(a_path, usecols=range(0, ndig_treino))
        An = np.true_divide(A, 255)
        g = decompose(An, p)
        np.savetxt("Trained/trained_" + str(i) + ".txt",g,fmt='%f')
        print("Finished training digit " + str(i) + ".   Elapsed time:", "{0:.1f}".format(time.time()-tic), "sec")
    toc = time.time()
    print("Total training time:", "{0:.1f}".format(toc - tic), "sec")
    
'''
Parte 2 - Classificação
'''
if sys.argv[1] == "class":
    n_test = int(input("Escolha n_test (entre 1 e 10mil): "))
    print("Classifying...")
    tic = time.time() 
    path = "dados_mnist/test_images.txt"
    A = np.loadtxt("dados_mnist/test_images.txt", usecols=range(0, n_test)) # matriz que gaurda digitos a serem classificados
    W = [] # vetor que guarda as matrizes de treino de cada digito i na posicao W[i]
    H = [] # vetor que guarda as matrizes de solve(Wd, A) de cada digito i na posicao H[i]
    
    for i in range(10):
        w_path = "Trained/trained_" + str(i) + ".txt"
        W.append(np.loadtxt(w_path))

        h = solve(W[i].copy(), A.copy())
        H.append(h)
    W = np.array(W)
    H = np.array(H)
    
    p = H.shape[1]
    dig = [0]*n_test  # vetor que guarda o provável dígito
    erros  = [0.]*n_test # guardará o mínimo erro
    for k in range(n_test):
        erros_k = []
        for j in range(10):
            aux = np.subtract(A[0:l, k], np.matmul(W[j], H[j, 0:p , k]))
            erros_k.append(np.sqrt(np.sum(aux ** 2, axis=None)))
        erros_k = np.array(erros_k)
        minimum = np.argmin(erros_k)
        dig[k] = minimum
        erros[k] = erros_k[minimum]
    np.savetxt("Tested_indices.txt",dig,fmt='%d')
    toc = time.time() - tic
    print("Finished classification. \nTo see results, run 'python3 main_ex.py compare' \nElapsed time: ", "{0:.1f}".format(toc), "sec")
'''
Parte 3 - Averiguação:
'''
if sys.argv[1] == "compare":
    print("Comparing...")
    
    # carrega as classificações
    class_path = "Tested_indices.txt" 
    Q = np.loadtxt(class_path)
    
    # carrega o gabarito
    lines = Q.shape[0]
    t_path = "dados_mnist/test_index.txt"
    T = np.loadtxt(t_path, max_rows = lines)
    
    acertos = 0
    acertos_por_dig = [0]*10
    vezes_por_dig = [0]*10
    
    for i in range(lines):
        correto = int(T[i])
        vezes_por_dig[correto] += 1
        if int(Q[i]) == correto:
            acertos += 1
            acertos_por_dig[correto] += 1
    
    porcentagem_total = acertos / lines
    # laco for para nao dividir por 0
    porcentagem_por_dig = np.array([0.]*10)
    for i in range(10):
        if vezes_por_dig[i] != 0:
            porcentagem_por_dig[i] = acertos_por_dig[i] / vezes_por_dig[i]
        else:
            porcentagem_por_dig[i] = np.nan

    print("Acertos:", acertos, "de", str(lines))
    print("Porcentagem de acertos:", "{0:.2f}".format(100*porcentagem_total) , "%")
    print("Acertos por dígito: ")
    for i in range(10):
        print("Dígito", str(i) + ": acertos:", acertos_por_dig[i], "| ","{0:.2f}".format(100*porcentagem_por_dig[i]) , "%")
